import pandas as pd
import numpy as np

from sklearn.linear_model import LogisticRegressionCV
from sklearn.feature_selection import SelectFromModel

#clean data
df = pd.read_csv("/Users/xinzhang/Documents/2018 Fall/Project/master-project/data/dataset_15.csv", dtype=str, header=0)
df = df[df.Finished == 'TRUE']
df = df[pd.notnull(df['Q1.2'])]
df = df[pd.notnull(df['Q1.3'])]
df = df[pd.notnull(df['Q1.4'])]
df = df[pd.notnull(df['Q1.5'])]
df = df[pd.notnull(df['Q1.6'])]
df = df[pd.notnull(df['Q1.7'])]
df = df[pd.notnull(df['Q2.1'])]
df = df[pd.notnull(df['Q2.2'])]
df = df[pd.notnull(df['Q2.3'])]
df = df[pd.notnull(df['Q2.4'])]
df = df[pd.notnull(df['Q2.5'])]
df = df[pd.notnull(df['Q2.8_1'])]
df = df[pd.notnull(df['Q2.8_2'])]
df = df[pd.notnull(df['Q2.9_1'])]
df = df[pd.notnull(df['Q2.9_2'])]
df = df[pd.notnull(df['Q2.10_1'])]
df = df[pd.notnull(df['Q2.10_2'])]

for idx, row in df.iterrows():
    if row['Q1.9']:
        if pd.isnull(row['Q1.9']):
            df.at[idx, 'Q1.9'] = 'Change the property'
    if row['Q1.10']:
        if pd.isnull(row['Q1.10']):
            df.at[idx, 'Q1.10'] = 'No'

df = df[df['y'].str.slice(0, 7) != 'Neither']
for idx, row in df.iterrows():
    if row['y']:
        if row['y'] == 'Option 1 (left)':
            df.at[idx, 'y'] = 1
        elif row['y'] == 'Option 2 (right)':
            df.at[idx, 'y'] = 0


for idx, row in df.iterrows():
    if row['Q1.6']:
        if row['Q1.6'] == '5+ (Please write down the number)':
            df.at[idx, 'Q1.6'] = 5

for idx, row in df.iterrows():
    if row['Q1.7']:
        if row['Q1.7'] == 'less than 500 sq ft':
            df.at[idx, 'Q1.7'] = 5
        elif row['Q1.7'] == '500~1,000 sq ft':
            df.at[idx, 'Q1.7'] = 10
        elif row['Q1.7'] == '1,000~1,500 sq ft':
            df.at[idx, 'Q1.7'] = 15
        elif row['Q1.7'] == '1500~2,000 sq ft':
            df.at[idx, 'Q1.7'] = 20
        elif row['Q1.7'] == 'more than 2,000 sq ft':
            df.at[idx, 'Q1.7'] = 30

for idx, row in df.iterrows():
    if row['Q2.3']:
        if row['Q2.3'] == 'No':
            df.at[idx, 'Q2.3'] = 0
        elif row['Q2.3'] == 'Probably not':
            df.at[idx, 'Q2.3'] = 1
        elif row['Q2.3'] == 'Not Sure':
            df.at[idx, 'Q2.3'] = 2
        elif row['Q2.3'] == 'Probably yes':
            df.at[idx, 'Q2.3'] = 3
        elif row['Q2.3'] == 'Yes':
            df.at[idx, 'Q2.3'] = 4

df['Q1.6'] = df['Q1.6'].astype(np.int64)
df['Q1.7'] = df['Q1.7'].astype(np.int64)
df['y'] = df['y'].astype(np.int64)

#Create dummy variables
cat_vars=['Q1.2','Q1.3','Q1.4','Q1.5','Q1.9','Q1.10','Q2.1','Q2.2','Q2.4','Q2.5','Q2.8_1','Q2.8_2','Q2.9_1','Q2.9_2','Q2.10_1','Q2.10_2']
for var in cat_vars:
    cat_list='var'+'_'+var
    cat_list = pd.get_dummies(df[var], prefix=var)
    df1=df.join(cat_list)
    df=df1
cat_vars=['Finished','Q1.8','Q1.2','Q1.3','Q1.4','Q1.5','Q1.9','Q1.10','Q2.1','Q2.2','Q2.4','Q2.5','Q2.8_1','Q2.8_2','Q2.9_1','Q2.9_2','Q2.10_1','Q2.10_2']
df_vars=df.columns.values.tolist()
to_keep=[i for i in df_vars if i not in cat_vars]
df_final=df[to_keep]
#print(df_final.columns.values)
X = df_final.loc[:, df_final.columns != 'y']
y = df_final.loc[:, df_final.columns == 'y']
#print(df_final.dtypes)

#print(X.shape)
# why Leave One Out not work
logreg = LogisticRegressionCV(solver='liblinear', cv=10, penalty='l1').fit(X, y.values.ravel())
#logreg = LogisticRegressionCV(solver='liblinear', cv=200, penalty='l1', tol=0.1).fit(X, y.values.ravel())

y_pred = logreg.predict(X)
print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(logreg.score(X, y)))
#
# model = SelectFromModel(logreg, prefit=True)
# X_new = model.transform(X)
# print(X.columns[model.get_support()])
