import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt
import seaborn as sns
from imblearn.over_sampling import SMOTE
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import RFE
import statsmodels.api as sm
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

#clean data
df = pd.read_csv("/Users/xinzhang/Documents/2018 Fall/Project/master-project/data/dataset_1.csv", dtype=str, header=0)
df = df[df.Finished == 'TRUE']
df = df[pd.notnull(df['Q1.2'])]
df = df[pd.notnull(df['Q1.3'])]
df = df[pd.notnull(df['Q1.4'])]
df = df[pd.notnull(df['Q1.5'])]
df = df[pd.notnull(df['Q1.6'])]
df = df[pd.notnull(df['Q1.7'])]
df = df[pd.notnull(df['Q2.1'])]
df = df[pd.notnull(df['Q2.2'])]
df = df[pd.notnull(df['Q2.3'])]
df = df[pd.notnull(df['Q2.4'])]
df = df[pd.notnull(df['Q2.5'])]
df = df[pd.notnull(df['Q2.8_1'])]
df = df[pd.notnull(df['Q2.8_2'])]
df = df[pd.notnull(df['Q2.9_1'])]
df = df[pd.notnull(df['Q2.9_2'])]
df = df[pd.notnull(df['Q2.10_1'])]
df = df[pd.notnull(df['Q2.10_2'])]

for idx, row in df.iterrows():
    if row['Q1.9']:
        if pd.isnull(row['Q1.9']):
            df.at[idx, 'Q1.9'] = 'Change the property'
    if row['Q1.10']:
        if pd.isnull(row['Q1.10']):
            df.at[idx, 'Q1.10'] = 'No'

df = df[df['y'].str.slice(0, 7) != 'Neither']
for idx, row in df.iterrows():
    if row['y']:
        if row['y'] == 'Option 1 (left)':
            df.at[idx, 'y'] = 1
        elif row['y'] == 'Option 2 (right)':
            df.at[idx, 'y'] = 0


for idx, row in df.iterrows():
    if row['Q1.6']:
        if row['Q1.6'] == '5+ (Please write down the number)':
            df.at[idx, 'Q1.6'] = 5

for idx, row in df.iterrows():
    if row['Q1.7']:
        if row['Q1.7'] == 'less than 500 sq ft':
            df.at[idx, 'Q1.7'] = 5
        elif row['Q1.7'] == '500~1,000 sq ft':
            df.at[idx, 'Q1.7'] = 10
        elif row['Q1.7'] == '1,000~1,500 sq ft':
            df.at[idx, 'Q1.7'] = 15
        elif row['Q1.7'] == '1500~2,000 sq ft':
            df.at[idx, 'Q1.7'] = 20
        elif row['Q1.7'] == 'more than 2,000 sq ft':
            df.at[idx, 'Q1.7'] = 30

for idx, row in df.iterrows():
    if row['Q2.3']:
        if row['Q2.3'] == 'No':
            df.at[idx, 'Q2.3'] = 0
        elif row['Q2.3'] == 'Probably not':
            df.at[idx, 'Q2.3'] = 1
        elif row['Q2.3'] == 'Not Sure':
            df.at[idx, 'Q2.3'] = 2
        elif row['Q2.3'] == 'Probably yes':
            df.at[idx, 'Q2.3'] = 3
        elif row['Q2.3'] == 'Yes':
            df.at[idx, 'Q2.3'] = 4

df['Q1.6'] = df['Q1.6'].astype(np.int64)
df['Q1.7'] = df['Q1.7'].astype(np.int64)
df['y'] = df['y'].astype(np.int64)

#observe data

# count_0 = len(df[df['y']==0])
# count_1 = len(df[df['y']==1])
# pct_of_0 = count_0/(count_0+count_1)
# print("percentage of 0 is", pct_of_0*100)
# pct_of_1 = count_1/(count_0+count_1)
# print("percentage of 1 is", pct_of_1*100)
# print(df.groupby('y').mean())
# print(df.groupby('y').mean())
# print(df.groupby('Q1.2').mean())
# print(df.groupby('Q1.3').mean())
# print(df.groupby('Q1.4').mean())
# print(df.groupby('Q1.5').mean())
# print(df.groupby('Q1.6').mean())
# print(df.groupby('Q1.7').mean())
# print(df.groupby('Q1.9').mean())
# print(df.groupby('Q1.10').mean())
# print(df.groupby('Q2.1').mean())
# print(df.groupby('Q2.2').mean())
# print(df.groupby('Q2.3').mean())
# print(df.groupby('Q2.4').mean())
# print(df.groupby('Q2.5').mean())
# print(df.groupby('Q2.8_1').mean())
# print(df.groupby('Q2.8_2').mean())
# print(df.groupby('Q2.9_1').mean())
# print(df.groupby('Q2.9_2').mean())
# print(df.groupby('Q2.10_1').mean())
# print(df.groupby('Q2.10_2').mean())

#Visualizations
# plt.rc("font", size=14)
# sns.set(style="white")
# sns.set(style="whitegrid", color_codes=True)
#Q1.2 seems not good predictor
# pd.crosstab(df['Q1.2'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q1.2')
# plt.xlabel('Q1.2')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre12')

# seems good predictor
# pd.crosstab(df['Q1.3'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q1.3')
# plt.xlabel('Q1.3')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre13')

# seems not good predictor
# pd.crosstab(df['Q1.4'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q1.4')
# plt.xlabel('Q1.4')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre14')

# seems good predictor
# pd.crosstab(df['Q1.5'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q1.5')
# plt.xlabel('Q1.5')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre15')

#seems good predictor
# pd.crosstab(df['Q1.9'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q1.9')
# plt.xlabel('Q1.9')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre19')
#
# #seems good predictor
# pd.crosstab(df['Q1.10'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q1.10')
# plt.xlabel('Q1.10')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre110')

# seems good predictor
# pd.crosstab(df['Q2.1'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.1')
# plt.xlabel('Q2.1')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre21')

# seems not good predictor
# pd.crosstab(df['Q2.2'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.2')
# plt.xlabel('Q2.2')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre22')

# # seems good predictor
# pd.crosstab(df['Q2.3'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.3')
# plt.xlabel('Q2.3')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre23')
#
# # seems not good predictor
# pd.crosstab(df['Q2.4'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.4')
# plt.xlabel('Q2.4')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre24')
#
# # seems good predictor
# pd.crosstab(df['Q2.5'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.5')
# plt.xlabel('Q2.5')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre25')
#
# # seems good predictor
# pd.crosstab(df['Q2.8_1'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.8_1')
# plt.xlabel('Q2.8_1')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre281')
#
# # seems good predictor
# pd.crosstab(df['Q2.8_2'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.8_2')
# plt.xlabel('Q2.8_2')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre282')
#
# # seems not good predictor
# pd.crosstab(df['Q2.9_1'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.9_1')
# plt.xlabel('Q2.9_1')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre291')
#
# # seems not good predictor
# pd.crosstab(df['Q2.9_2'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.9_2')
# plt.xlabel('Q2.9_2')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre292')
#
# # seems not good predictor
# pd.crosstab(df['Q2.10_1'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.10_1')
# plt.xlabel('Q2.10_1')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre2101')
#
# # seems good predictor
# pd.crosstab(df['Q2.10_2'],df['y']).plot(kind='bar')
# plt.title('Frequency for Q2.10_2')
# plt.xlabel('Q2.10_2')
# plt.ylabel('Frequency of 0/1')
# plt.savefig('fre2102')

#Create dummy variables
cat_vars=['Q1.2','Q1.3','Q1.4','Q1.5','Q1.9','Q1.10','Q2.1','Q2.2','Q2.4','Q2.5','Q2.8_1','Q2.8_2','Q2.9_1','Q2.9_2','Q2.10_1','Q2.10_2']
for var in cat_vars:
    cat_list='var'+'_'+var
    cat_list = pd.get_dummies(df[var], prefix=var)
    df1=df.join(cat_list)
    df=df1
cat_vars=['Finished','Q1.8','Q1.2','Q1.3','Q1.4','Q1.5','Q1.9','Q1.10','Q2.1','Q2.2','Q2.4','Q2.5','Q2.8_1','Q2.8_2','Q2.9_1','Q2.9_2','Q2.10_1','Q2.10_2']
df_vars=df.columns.values.tolist()
to_keep=[i for i in df_vars if i not in cat_vars]
df_final=df[to_keep]
#print(df_final.columns.values)
X = df_final.loc[:, df_final.columns != 'y']
y = df_final.loc[:, df_final.columns == 'y']
#print(df_final.dtypes)


#Over-sampling using SMOTE
os = SMOTE(random_state=0)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)
columns = X_train.columns
os_data_X,os_data_y=os.fit_sample(X_train, y_train.values.ravel())
os_data_X = pd.DataFrame(data=os_data_X,columns=columns )
os_data_y= pd.DataFrame(data=os_data_y,columns=['y'])
# we can Check the numbers of our data
# print("length of oversampled data is ",len(os_data_X))
# print("Number of 0 in oversampled data",len(os_data_y[os_data_y['y']==0]))
# print("Number of 1",len(os_data_y[os_data_y['y']==1]))
# print("Proportion of no 0 in oversampled data is ",len(os_data_y[os_data_y['y']==0])/len(os_data_X))
# print("Proportion of 1 in oversampled data is ",len(os_data_y[os_data_y['y']==1])/len(os_data_X))
#

#Recursive Feature Elimination
df_final_vars=df_final.columns.values.tolist()
y=['y']
X=[i for i in df_final_vars if i not in y]
print(X)
logreg = LogisticRegression(solver='liblinear')
# I am not sure how many features need to be selected
rfe = RFE(logreg, 10)
rfe = rfe.fit(os_data_X, os_data_y.values.ravel())
print(rfe.support_)
print(rfe.ranking_)
cols=[X[6], X[9], X[11], X[17], X[19], X[31], X[35], X[49], X[51], X[53]]
# #print(cols)
#
# #Implementing the model
# X=os_data_X[cols]
# y=os_data_y['y']
#
# logit_model=sm.Logit(y,X)
# result=logit_model.fit()
# print(result.summary2())

cols_2=[X[6], X[11], X[51], X[53]]
X_2 = os_data_X[cols_2]
y_2 = os_data_y['y']
logit_model=sm.Logit(y_2,X_2)
result_2=logit_model.fit()
print(result_2.summary2())
#
#
#Logistic Regression Model Fitting
logreg.fit(X_train, y_train.values.ravel())
y_pred = logreg.predict(X_test)
print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(logreg.score(X_test, y_test)))
#
#Confusion Matrix
confusion_matrix = confusion_matrix(y_test, y_pred)
print(confusion_matrix)
#
#Compute precision, recall, F-measure and support
print(classification_report(y_test, y_pred))
