import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import numpy as np

#clean data
df = pd.read_csv("/Users/xinzhang/Documents/2018 Fall/Project/master-project/data/dataset_3.csv", dtype=str, header=0)
df = df[df.Finished == 'TRUE']
df = df[pd.notnull(df['Q1.2'])]
df = df[pd.notnull(df['Q1.3'])]
df = df[pd.notnull(df['Q1.4'])]
df = df[pd.notnull(df['Q1.5'])]
df = df[pd.notnull(df['Q1.6'])]
df = df[pd.notnull(df['Q1.7'])]
df = df[pd.notnull(df['Q2.1'])]
df = df[pd.notnull(df['Q2.2'])]
df = df[pd.notnull(df['Q2.3'])]
df = df[pd.notnull(df['Q2.4'])]
df = df[pd.notnull(df['Q2.5'])]
df = df[pd.notnull(df['Q2.8_1'])]
df = df[pd.notnull(df['Q2.8_2'])]
df = df[pd.notnull(df['Q2.9_1'])]
df = df[pd.notnull(df['Q2.9_2'])]
df = df[pd.notnull(df['Q2.10_1'])]
df = df[pd.notnull(df['Q2.10_2'])]

for idx, row in df.iterrows():
    if row['Q1.9']:
        if pd.isnull(row['Q1.9']):
            df.at[idx, 'Q1.9'] = 'Change the property'
    if row['Q1.10']:
        if pd.isnull(row['Q1.10']):
            df.at[idx, 'Q1.10'] = 'No'

df = df[df['y'].str.slice(0, 7) != 'Neither']
for idx, row in df.iterrows():
    if row['y']:
        if row['y'] == 'Option 1 (left)':
            df.at[idx, 'y'] = 1
        elif row['y'] == 'Option 2 (right)':
            df.at[idx, 'y'] = 0


for idx, row in df.iterrows():
    if row['Q1.6']:
        if row['Q1.6'] == '5+ (Please write down the number)':
            df.at[idx, 'Q1.6'] = 5

for idx, row in df.iterrows():
    if row['Q1.7']:
        if row['Q1.7'] == 'less than 500 sq ft':
            df.at[idx, 'Q1.7'] = 5
        elif row['Q1.7'] == '500~1,000 sq ft':
            df.at[idx, 'Q1.7'] = 10
        elif row['Q1.7'] == '1,000~1,500 sq ft':
            df.at[idx, 'Q1.7'] = 15
        elif row['Q1.7'] == '1500~2,000 sq ft':
            df.at[idx, 'Q1.7'] = 20
        elif row['Q1.7'] == 'more than 2,000 sq ft':
            df.at[idx, 'Q1.7'] = 30

for idx, row in df.iterrows():
    if row['Q2.3']:
        if row['Q2.3'] == 'No':
            df.at[idx, 'Q2.3'] = 0
        elif row['Q2.3'] == 'Probably not':
            df.at[idx, 'Q2.3'] = 1
        elif row['Q2.3'] == 'Not Sure':
            df.at[idx, 'Q2.3'] = 2
        elif row['Q2.3'] == 'Probably yes':
            df.at[idx, 'Q2.3'] = 3
        elif row['Q2.3'] == 'Yes':
            df.at[idx, 'Q2.3'] = 4

df['Q1.6'] = df['Q1.6'].astype(np.int64)
df['Q1.7'] = df['Q1.7'].astype(np.int64)
df['y'] = df['y'].astype(np.int64)



#df.to_csv("newdata3.csv")

df = pd.read_csv("/Users/xinzhang/Documents/2018 Fall/Project/master-project/data/newdata3.csv", dtype=str, header=0)

X = df.loc[:, df.columns != 'y']
y = df.loc[:, df.columns == 'y']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

# print ("Train_x Shape :: ", X_train.shape)
# print ("Train_y Shape :: ", X_test.shape)
# print ("Test_x Shape :: ", y_train.shape)
# print ("Test_y Shape :: ", y_test.shape)

#print(X_train)

clf = RandomForestClassifier()
clf.fit(X_train, y_train)